﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System.Threading;
using Core.Net;
using System.Linq;

namespace Server
{
    class Server
    {
        class ErrorSignifier
        {
            public bool Error
            {
                get
                {
                    return error;
                }
                set
                {
                    if (!error)
                        error = value;
                }
            }
            bool error = false;
        }

        private Core.Net.ClientTcp tcpClient;
        private List<Core.Net.ClientTcp> tcpClientList = new List<Core.Net.ClientTcp>();

        private Core.Db.DbUtills dbUtills = new Core.Db.DbUtills();

        public Server()
        {
        }

        public void Start()
        {
            TcpListener server = null;

            try
            {
                int port = 13000;

                server = new TcpListener(IPAddress.Any, port);

                server.Start();

                Byte[] bytes = new Byte[256];

                while (true)
                {
                    Console.WriteLine("Waiting for a connection... ");

                    tcpClient = new Core.Net.ClientTcp(server.AcceptTcpClient());
                    tcpClient.OnReceive += new Core.Net.ClientTcp.OnReceiveHandler(Client_OnReceive);
                    tcpClientList.Add(tcpClient);

                    Console.WriteLine("Connected");
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: " + e);
            }
            finally
            {
                server.Stop();
            }
        }

        private bool UserRecognized(ClientTcp clientTcp, string login, string password)
        {
            var users = dbUtills.GetDbObject<Core.Type.Db.User>().FindAll(x => x.Name == login && x.PasswordHash == password);
            if (users.Count == 1)
            {
                clientTcp.IsUserApproved = true;
                clientTcp.UserId = (int)users[0].Id;
                return true;
            }
            else
                return false;
        }

        private bool CreateUser(string login, string password)
        {
            Core.Type.Db.User newUser = new Core.Type.Db.User() { Name = login, Confirmed = true, PasswordHash = password };
            return dbUtills.InsertObject<Core.Type.Db.User>(newUser);
        }

        private bool CreateVote(long ticks)
        {
            Core.Type.Db.Vote newVote = new Core.Type.Db.Vote() { Start = DateTime.UtcNow, End = new DateTime(ticks) };
            return dbUtills.InsertObject<Core.Type.Db.Vote>(newVote);
        }

        private bool CreateVoix(int voteId, int restaurantId, int userId)
        {
            var voixUserAndVote = dbUtills.GetDbObject<Core.Type.Db.Voix>().Where(x => x.IdUser == userId && x.IdVote == voteId).ToList();
            if(voixUserAndVote.Count == 1)
            {
                if (voixUserAndVote[0].IdRestaurant == restaurantId)
                    return true;
                dbUtills.DeleteObject<Core.Type.Db.Voix>(voixUserAndVote[0]);
            }
            var newVoix = new Core.Type.Db.Voix() { IdRestaurant = restaurantId, IdUser = userId, IdVote = voteId };
            return dbUtills.InsertObject<Core.Type.Db.Voix>(newVoix);
        }

        private bool CreateRestaurant(string restaurantName, string restaurantDescription)
        {
            Core.Type.Db.Restaurant newVote = new Core.Type.Db.Restaurant() { Name = restaurantName, Description = restaurantDescription };
            return dbUtills.InsertObject<Core.Type.Db.Restaurant>(newVote);
        }

        private string StrDataRefresh(int idUser)
        {
            Core.Type.CentralizedData data = dbUtills.GetCentralizedData(idUser);
            return data.ToXmlStr();
        }
        public void Client_OnReceive(Core.Net.ClientTcp src, string s)
        {
            string[] dataList = s.Trim().Split(';');
            if (Enum.TryParse<Protocol>(dataList[0], out Protocol protocol))
            {
                switch (protocol)
                {
                    case Core.Net.Protocol.Connnection:
                        if (UserRecognized(src, dataList[1], dataList[2]))
                        {
                            src.Send(String.Join(';', protocol, Protocol.Approved));
                        }
                        else
                        {
                            src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        }
                        break;
                    case Core.Net.Protocol.Disconnection:
                        if (src.IsUserApproved)
                        {
                            src.IsUserApproved = false;
                            src.Send(String.Join(';', protocol, Protocol.Approved));
                        }
                        else
                        {
                            src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        }
                        break;
                    case Core.Net.Protocol.Register:
                        if (CreateUser(dataList[1], dataList[2]))
                        {
                            src.Send(String.Join(';', protocol, Protocol.Approved));
                        }
                        else
                        {
                            src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        }
                        break;
                    case Core.Net.Protocol.Refresh:
                        ErrorSignifier output = new ErrorSignifier();
                        if (src.IsUserApproved)
                        {
                            src.Send(String.Join(';', protocol, StrDataRefresh((int)src.UserId)));
                            break;
                        }
                        src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        break;
                    case Core.Net.Protocol.Vote:
                        if (src.IsUserApproved)
                        {
                            if(int.TryParse(dataList[1], out int voteId) && int.TryParse(dataList[2], out int restaurantId))
                            {
                                CreateVoix(voteId, restaurantId, (int)tcpClient.UserId);
                                src.Send(String.Join(';', protocol, Protocol.Approved));
                                break;
                            }
                        }
                        src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        break;
                    case Core.Net.Protocol.NewVote:
                        if (src.IsUserApproved)
                        {
                            if (long.TryParse(dataList[1], out long dateEnd))
                            {
                                if (CreateVote(dateEnd))
                                {
                                    src.Send(String.Join(';', protocol, Protocol.Approved));
                                    break;
                                }
                            }
                        }
                        src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        break;
                    case Protocol.NewRestaurant:
                        if (src.IsUserApproved)
                        {
                            if (CreateRestaurant(dataList[1], dataList[2]))
                            {
                                src.Send(String.Join(';', protocol, Protocol.Approved));
                                break;
                            }
                        }
                        src.Send(String.Join(';', protocol, Protocol.Disapproved));
                        break;
                    case Protocol.Approved:
                        break;
                    case Protocol.Disapproved:
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
