﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Client
{
    public partial class FormLogin : Form
    {
        ClientComm client = new ClientComm();
        HashAlgorithm algorithm = SHA256.Create();
        public FormLogin()
        {
            InitializeComponent();
        }

        private void connection(object sender, EventArgs e)
        {
            if (Control.ModifierKeys == Keys.Shift)
            {
                client.SendRegister(textBoxLogin.Text, textBoxPassword.Text);
            }
            else
            {
                if (client.SendConnection(textBoxLogin.Text, textBoxPassword.Text))
                {
                    this.Visible = false;
                    FormMainClient formMainClient = new FormMainClient(client);
                    formMainClient.ShowDialog();
                    this.Visible = true;
                }


            }
        }

        private void closed(object sender, EventArgs e)
        {
            client.Dispose();
            Application.Exit();
        }
    }
}
