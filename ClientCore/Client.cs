﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Core.Net;

namespace Client
{
    public class ClientComm
    {
        public OnRefreshHandler OnRefresh;
        public delegate void OnRefreshHandler(Core.Type.CentralizedData data);

        Core.Net.ClientTcp clientTcp = new Core.Net.ClientTcp("10.3.0.119", 13000);
        List<WaitedApprovement> waitedAcks = new List<WaitedApprovement>();
        readonly TimeSpan timeOut = new TimeSpan(0, 0, 10);

        Core.Type.CentralizedData centralizedData = new Core.Type.CentralizedData();//Core.Type.CentralizedData.GetExemple();
        public ref Core.Type.CentralizedData CentralizedData
        {
            get
            {
                lock(centralizedData)
                {
                    return ref centralizedData;
                }
            }
        }

        class WaitedApprovement
        {
            public Core.Net.Protocol protocol;
            public Nullable<bool> ackited;
        }

        public ClientComm()
        {
            clientTcp.OnReceive += _OnReceive;
        }

        void _OnReceive(Core.Net.ClientTcp clientTcp, string str)
        {
            string[] dataList = str.Trim().Split(';');
            if (Enum.TryParse<Protocol>(dataList[0], out Protocol protocol))
            {
                if (dataList.Length > 1 && aprovement(protocol, dataList[1] == Protocol.Disapproved.ToString()))
                    return;
                switch (protocol)
                {
                    case Protocol.Approved:
                        break;
                    case Protocol.Disapproved:
                        break;
                    case Protocol.Connnection:
                        break;
                    case Protocol.Disconnection:
                        break;
                    case Protocol.Register:
                        break;
                    case Protocol.Vote:
                        break;
                    case Protocol.Refresh:
                        lock (centralizedData)
                        {
                            centralizedData = Core.Type.CentralizedData.FromXmlStr(dataList[1]);
                            OnRefresh?.Invoke(centralizedData);
                        }
                        break;
                    case Protocol.NewVote:
                        break;
                    case Protocol.NewRestaurant:
                        break;
                }
            }
        }

        public bool SendConnection(string login, string password)
        {
            clientTcp.Send(String.Join(';', Core.Net.Protocol.Connnection, login, GetHashString(password)));
            var reponse = WaitApprovement(Protocol.Connnection);
            return reponse == true;
        }

        public bool SendRegister(string login, string password)
        {
            clientTcp.Send(String.Join(';', Core.Net.Protocol.Register, login, GetHashString(password)));
            var reponse = WaitApprovement(Protocol.Register);
            return reponse == true;
        }

        public bool SendDisconnect()
        {
            clientTcp.Send(Core.Net.Protocol.Disconnection.ToString());
            var reponse = WaitApprovement(Protocol.Disconnection);
            return reponse == true;
        }

        public bool SendCreateVote(DateTime end)
        {
            clientTcp.Send(String.Join(';', Core.Net.Protocol.NewVote.ToString(), end.Ticks));
            var reponse = WaitApprovement(Protocol.NewVote);
            return reponse == true;
        }

        public bool SendCreateRestaurant(string restaurantName, string restaurantDescription)
        {
            clientTcp.Send(String.Join(';', Core.Net.Protocol.NewRestaurant.ToString(), restaurantName, restaurantDescription));
            var reponse = WaitApprovement(Protocol.NewRestaurant);
            return reponse == true;
        }

        public bool SendRefresh()
        {
            clientTcp.Send(String.Join(';', Core.Net.Protocol.Refresh.ToString()));
            var reponse = WaitApprovement(Protocol.Refresh);
            return reponse == true;
        }

        public bool SendVote(int voteId, int restaurantId)
        {
            clientTcp.Send(String.Join(';', Core.Net.Protocol.Vote.ToString(), voteId, restaurantId));
            var reponse = WaitApprovement(Protocol.Vote);
            SendRefresh();
            return reponse == true;
        }

        private Nullable<bool> WaitApprovement(Protocol protocol)
        {
            var approvement = new WaitedApprovement { protocol = protocol };
            lock(waitedAcks)
            {
                waitedAcks.Add(approvement);
            }
            DateTime start = DateTime.UtcNow;
            while(approvement.ackited == null && start + timeOut > DateTime.UtcNow)
            {
                Thread.Sleep(100);
            }

            lock(waitedAcks)
            {
                waitedAcks.Remove(approvement);
            }

            return approvement.ackited;
        }

        private bool aprovement(Protocol protocol, bool disaproved)
        {
            lock(waitedAcks)
            {
                foreach(var ack in waitedAcks)
                {
                    if(ack.protocol == protocol)
                    {
                        ack.ackited = !disaproved;
                        return disaproved;
                    }
                }
            }
            return disaproved;
        }

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public void Dispose()
        {
            clientTcp.Dispose();
        }
    }
}
