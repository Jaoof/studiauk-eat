﻿namespace Client
{
    partial class FormMainClient
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonDisconnect = new System.Windows.Forms.ToolStripButton();
            this.tabPageHistoric = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.listBoxFinished = new System.Windows.Forms.ListBox();
            this.labelWinner = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewFinished = new System.Windows.Forms.DataGridView();
            this.tabPageCreateVote = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCreateVote = new System.Windows.Forms.Button();
            this.dateTimePickerNewVote = new System.Windows.Forms.DateTimePicker();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBoxCurrent = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.labelDataSended = new System.Windows.Forms.Label();
            this.labelTimeLeft = new System.Windows.Forms.Label();
            this.dataGridViewCurrent = new System.Windows.Forms.DataGridView();
            this.tabControlVote = new System.Windows.Forms.TabControl();
            this.tabPageAddRestaurant = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonAddRestaurant = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxRestaurantName = new System.Windows.Forms.TextBox();
            this.richTextBoxRestaurantDescription = new System.Windows.Forms.RichTextBox();
            this.timerRefreshData = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.tabPageHistoric.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFinished)).BeginInit();
            this.tabPageCreateVote.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCurrent)).BeginInit();
            this.tabControlVote.SuspendLayout();
            this.tabPageAddRestaurant.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonDisconnect});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(878, 27);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonDisconnect
            // 
            this.toolStripButtonDisconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDisconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDisconnect.Name = "toolStripButtonDisconnect";
            this.toolStripButtonDisconnect.Size = new System.Drawing.Size(86, 24);
            this.toolStripButtonDisconnect.Text = "Disconnect";
            this.toolStripButtonDisconnect.Click += new System.EventHandler(this.ToolStripButtonDisconnect_Click);
            // 
            // tabPageHistoric
            // 
            this.tabPageHistoric.Controls.Add(this.splitContainer2);
            this.tabPageHistoric.Location = new System.Drawing.Point(4, 25);
            this.tabPageHistoric.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPageHistoric.Name = "tabPageHistoric";
            this.tabPageHistoric.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPageHistoric.Size = new System.Drawing.Size(870, 434);
            this.tabPageHistoric.TabIndex = 2;
            this.tabPageHistoric.Text = "Historique des votes";
            this.tabPageHistoric.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 2);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.listBoxFinished);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.labelWinner);
            this.splitContainer2.Panel2.Controls.Add(this.label2);
            this.splitContainer2.Panel2.Controls.Add(this.dataGridViewFinished);
            this.splitContainer2.Size = new System.Drawing.Size(864, 430);
            this.splitContainer2.SplitterDistance = 172;
            this.splitContainer2.TabIndex = 0;
            // 
            // listBoxFinished
            // 
            this.listBoxFinished.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxFinished.FormattingEnabled = true;
            this.listBoxFinished.ItemHeight = 16;
            this.listBoxFinished.Location = new System.Drawing.Point(0, 0);
            this.listBoxFinished.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBoxFinished.Name = "listBoxFinished";
            this.listBoxFinished.Size = new System.Drawing.Size(172, 430);
            this.listBoxFinished.TabIndex = 0;
            this.listBoxFinished.SelectedIndexChanged += new System.EventHandler(this.ListBoxFinished_SelectedIndexChanged);
            // 
            // labelWinner
            // 
            this.labelWinner.AutoSize = true;
            this.labelWinner.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWinner.Location = new System.Drawing.Point(12, 15);
            this.labelWinner.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWinner.Name = "labelWinner";
            this.labelWinner.Size = new System.Drawing.Size(115, 29);
            this.labelWinner.TabIndex = 2;
            this.labelWinner.Text = "Gagnant :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 1;
            // 
            // dataGridViewFinished
            // 
            this.dataGridViewFinished.AllowUserToAddRows = false;
            this.dataGridViewFinished.AllowUserToDeleteRows = false;
            this.dataGridViewFinished.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFinished.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFinished.Location = new System.Drawing.Point(3, 49);
            this.dataGridViewFinished.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewFinished.Name = "dataGridViewFinished";
            this.dataGridViewFinished.ReadOnly = true;
            this.dataGridViewFinished.RowHeadersWidth = 51;
            this.dataGridViewFinished.RowTemplate.Height = 24;
            this.dataGridViewFinished.Size = new System.Drawing.Size(511, 207);
            this.dataGridViewFinished.TabIndex = 0;
            this.dataGridViewFinished.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewFinished_CellClick);
            // 
            // tabPageCreateVote
            // 
            this.tabPageCreateVote.Controls.Add(this.label1);
            this.tabPageCreateVote.Controls.Add(this.buttonCreateVote);
            this.tabPageCreateVote.Controls.Add(this.dateTimePickerNewVote);
            this.tabPageCreateVote.Location = new System.Drawing.Point(4, 25);
            this.tabPageCreateVote.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageCreateVote.Name = "tabPageCreateVote";
            this.tabPageCreateVote.Size = new System.Drawing.Size(870, 434);
            this.tabPageCreateVote.TabIndex = 3;
            this.tabPageCreateVote.Text = "Créer un vote";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Date de fin";
            // 
            // buttonCreateVote
            // 
            this.buttonCreateVote.Location = new System.Drawing.Point(177, 36);
            this.buttonCreateVote.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCreateVote.Name = "buttonCreateVote";
            this.buttonCreateVote.Size = new System.Drawing.Size(100, 28);
            this.buttonCreateVote.TabIndex = 2;
            this.buttonCreateVote.Text = "Créer";
            this.buttonCreateVote.UseVisualStyleBackColor = true;
            this.buttonCreateVote.Click += new System.EventHandler(this.ButtonCreateVote_Click);
            // 
            // dateTimePickerNewVote
            // 
            this.dateTimePickerNewVote.CustomFormat = "HH:mm:ss dd/MM/yy";
            this.dateTimePickerNewVote.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerNewVote.Location = new System.Drawing.Point(107, 4);
            this.dateTimePickerNewVote.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerNewVote.Name = "dateTimePickerNewVote";
            this.dateTimePickerNewVote.Size = new System.Drawing.Size(169, 22);
            this.dateTimePickerNewVote.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(870, 434);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Vote en cours";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 2);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listBoxCurrent);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.button1);
            this.splitContainer1.Panel2.Controls.Add(this.labelDataSended);
            this.splitContainer1.Panel2.Controls.Add(this.labelTimeLeft);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewCurrent);
            this.splitContainer1.Size = new System.Drawing.Size(864, 430);
            this.splitContainer1.SplitterDistance = 187;
            this.splitContainer1.TabIndex = 0;
            // 
            // listBoxCurrent
            // 
            this.listBoxCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxCurrent.FormattingEnabled = true;
            this.listBoxCurrent.ItemHeight = 16;
            this.listBoxCurrent.Location = new System.Drawing.Point(0, 0);
            this.listBoxCurrent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBoxCurrent.Name = "listBoxCurrent";
            this.listBoxCurrent.Size = new System.Drawing.Size(187, 430);
            this.listBoxCurrent.TabIndex = 0;
            this.listBoxCurrent.SelectedIndexChanged += new System.EventHandler(this.currentListBoxSelectedChange);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(389, 305);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 53);
            this.button1.TabIndex = 3;
            this.button1.Text = "Voter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ButtonVote_Click);
            // 
            // labelDataSended
            // 
            this.labelDataSended.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDataSended.AutoSize = true;
            this.labelDataSended.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDataSended.ForeColor = System.Drawing.Color.ForestGreen;
            this.labelDataSended.Location = new System.Drawing.Point(54, 317);
            this.labelDataSended.Name = "labelDataSended";
            this.labelDataSended.Size = new System.Drawing.Size(145, 29);
            this.labelDataSended.TabIndex = 2;
            this.labelDataSended.Text = "Vote envoyé";
            this.labelDataSended.Visible = false;
            // 
            // labelTimeLeft
            // 
            this.labelTimeLeft.AutoSize = true;
            this.labelTimeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeLeft.Location = new System.Drawing.Point(8, 7);
            this.labelTimeLeft.Name = "labelTimeLeft";
            this.labelTimeLeft.Size = new System.Drawing.Size(217, 32);
            this.labelTimeLeft.TabIndex = 1;
            this.labelTimeLeft.Text = "Temps restant : ";
            // 
            // dataGridViewCurrent
            // 
            this.dataGridViewCurrent.AllowUserToAddRows = false;
            this.dataGridViewCurrent.AllowUserToDeleteRows = false;
            this.dataGridViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCurrent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCurrent.Location = new System.Drawing.Point(14, 49);
            this.dataGridViewCurrent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewCurrent.Name = "dataGridViewCurrent";
            this.dataGridViewCurrent.RowHeadersWidth = 51;
            this.dataGridViewCurrent.RowTemplate.Height = 24;
            this.dataGridViewCurrent.ShowEditingIcon = false;
            this.dataGridViewCurrent.Size = new System.Drawing.Size(565, 238);
            this.dataGridViewCurrent.StandardTab = true;
            this.dataGridViewCurrent.TabIndex = 0;
            this.dataGridViewCurrent.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.currentCellClicked);
            this.dataGridViewCurrent.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewCurrent_CellContentClick);
            this.dataGridViewCurrent.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewCurrent_CellValueChanged);
            // 
            // tabControlVote
            // 
            this.tabControlVote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlVote.Controls.Add(this.tabPage1);
            this.tabControlVote.Controls.Add(this.tabPageHistoric);
            this.tabControlVote.Controls.Add(this.tabPageCreateVote);
            this.tabControlVote.Controls.Add(this.tabPageAddRestaurant);
            this.tabControlVote.Location = new System.Drawing.Point(0, 30);
            this.tabControlVote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControlVote.Name = "tabControlVote";
            this.tabControlVote.SelectedIndex = 0;
            this.tabControlVote.Size = new System.Drawing.Size(878, 463);
            this.tabControlVote.TabIndex = 0;
            // 
            // tabPageAddRestaurant
            // 
            this.tabPageAddRestaurant.Controls.Add(this.label4);
            this.tabPageAddRestaurant.Controls.Add(this.buttonAddRestaurant);
            this.tabPageAddRestaurant.Controls.Add(this.label3);
            this.tabPageAddRestaurant.Controls.Add(this.textBoxRestaurantName);
            this.tabPageAddRestaurant.Controls.Add(this.richTextBoxRestaurantDescription);
            this.tabPageAddRestaurant.Location = new System.Drawing.Point(4, 25);
            this.tabPageAddRestaurant.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageAddRestaurant.Name = "tabPageAddRestaurant";
            this.tabPageAddRestaurant.Size = new System.Drawing.Size(870, 434);
            this.tabPageAddRestaurant.TabIndex = 4;
            this.tabPageAddRestaurant.Text = "Ajouter un restaurant";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Description du restaurant";
            // 
            // buttonAddRestaurant
            // 
            this.buttonAddRestaurant.Location = new System.Drawing.Point(640, 303);
            this.buttonAddRestaurant.Name = "buttonAddRestaurant";
            this.buttonAddRestaurant.Size = new System.Drawing.Size(75, 23);
            this.buttonAddRestaurant.TabIndex = 3;
            this.buttonAddRestaurant.Text = "Ajouter";
            this.buttonAddRestaurant.UseVisualStyleBackColor = true;
            this.buttonAddRestaurant.Click += new System.EventHandler(this.ButtonAddRestaurant_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nom du restaurant";
            // 
            // textBoxRestaurantName
            // 
            this.textBoxRestaurantName.Location = new System.Drawing.Point(140, 8);
            this.textBoxRestaurantName.Name = "textBoxRestaurantName";
            this.textBoxRestaurantName.Size = new System.Drawing.Size(100, 22);
            this.textBoxRestaurantName.TabIndex = 1;
            // 
            // richTextBoxRestaurantDescription
            // 
            this.richTextBoxRestaurantDescription.Location = new System.Drawing.Point(8, 57);
            this.richTextBoxRestaurantDescription.Name = "richTextBoxRestaurantDescription";
            this.richTextBoxRestaurantDescription.Size = new System.Drawing.Size(707, 240);
            this.richTextBoxRestaurantDescription.TabIndex = 0;
            this.richTextBoxRestaurantDescription.Text = "";
            // 
            // timerRefreshData
            // 
            this.timerRefreshData.Enabled = true;
            this.timerRefreshData.Interval = 5000;
            this.timerRefreshData.Tick += new System.EventHandler(this.TimerRefreshData_Tick);
            // 
            // FormMainClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 493);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControlVote);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormMainClient";
            this.Text = "Client";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMainClient_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPageHistoric.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFinished)).EndInit();
            this.tabPageCreateVote.ResumeLayout(false);
            this.tabPageCreateVote.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCurrent)).EndInit();
            this.tabControlVote.ResumeLayout(false);
            this.tabPageAddRestaurant.ResumeLayout(false);
            this.tabPageAddRestaurant.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonDisconnect;
        private System.Windows.Forms.TabPage tabPageHistoric;
        private System.Windows.Forms.TabPage tabPageCreateVote;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox listBoxFinished;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewFinished;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listBoxCurrent;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelDataSended;
        private System.Windows.Forms.Label labelTimeLeft;
        private System.Windows.Forms.DataGridView dataGridViewCurrent;
        private System.Windows.Forms.TabControl tabControlVote;
        private System.Windows.Forms.Label labelWinner;
        private System.Windows.Forms.Button buttonCreateVote;
        private System.Windows.Forms.DateTimePicker dateTimePickerNewVote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerRefreshData;
        private System.Windows.Forms.TabPage tabPageAddRestaurant;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonAddRestaurant;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRestaurantName;
        private System.Windows.Forms.RichTextBox richTextBoxRestaurantDescription;
    }
}

