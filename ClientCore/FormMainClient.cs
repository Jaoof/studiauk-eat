﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FormMainClient : Form
    {
        ClientComm client;

        Core.Type.DataVote dataVoteCurrent
        {
            get
            {
                return listBoxCurrent.SelectedItem as Core.Type.DataVote;
            }
        }

        Core.Type.DataVote dataVoteFinished
        {
            get
            {
                return listBoxFinished.SelectedItem as Core.Type.DataVote;
            }
        }

        public FormMainClient(ClientComm client)
        {
            client.SendRefresh();
            this.client = client;
            client.OnRefresh += onRefresh;
            InitializeComponent();
            listBoxCurrent.DataSource = client.CentralizedData.dataCurrentVotes;
            listBoxFinished.DataSource = client.CentralizedData.dataFinishedVotes;
        }

        private void currentListBoxSelectedChange(object sender, EventArgs e)
        {
            dataGridViewCurrent.DataSource = dataVoteCurrent?.VoteLane;
            labelTimeLeft.Text = labelTimeLeft.Text.Split(':')[0] + ":" + (dataVoteCurrent?.EndTime - DateTime.UtcNow);
        }

        private void currentCellClicked(object sender, DataGridViewCellEventArgs e)
        {
            var column = dataGridViewCurrent.Columns[e.ColumnIndex];
            if (column.Name == "NumberVote")
            {
                FormListUser formListUser = new FormListUser(dataVoteCurrent.VoteLane[e.RowIndex].usersWhoVoteForIt.Select(x => x.Name).ToList());
                formListUser.ShowDialog();
            }
            else if (column.Name == "RestaurantName")
            {
                FormRestaurantOverview formRestaurantOverview = new FormRestaurantOverview(dataVoteCurrent.VoteLane[e.RowIndex].RestaurantName, dataVoteCurrent.VoteLane[e.RowIndex].restaurantDescription);
                formRestaurantOverview.ShowDialog();
            }
        }

        private void ButtonCreateVote_Click(object sender, EventArgs e)
        {
            client.SendCreateVote(dateTimePickerNewVote.Value.ToUniversalTime());
        }

        private void hideLabelDataSended()
        {
            Thread.Sleep(1000);
            this.Invoke((MethodInvoker)(() => { labelWinner.Visible = false; }));
        }

        private void TimerRefreshData_Tick(object sender, EventArgs e)
        {
            client.SendRefresh();
        }

        private void ButtonAddRestaurant_Click(object sender, EventArgs e)
        {
            client.SendCreateRestaurant(textBoxRestaurantName.Text, richTextBoxRestaurantDescription.Text);
        }

        private void onRefresh(Core.Type.CentralizedData data)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new ClientComm.OnRefreshHandler(onRefresh), data);
            }
            else
            {
                refreshCurrent(data);
                refreshFinished(data);
            }
        }

        private void refreshCurrent(Core.Type.CentralizedData data)
        {
            bool voteSaved = false;
            bool selectionSaved = false;
            int cellRow = 0, cellCol = 0;
            Core.Type.DataVote vote = null;
            if (dataVoteCurrent != null)
            {
                if (dataGridViewCurrent.SelectedCells.Count > 0)
                {
                    selectionSaved = true;
                    var cell = dataGridViewCurrent.SelectedCells[0];
                    cellRow = cell.RowIndex;
                    cellCol = cell.ColumnIndex;
                }
                vote = dataVoteCurrent.Copy();
                voteSaved = true;
            }
            listBoxCurrent.DataSource = data.dataCurrentVotes;
            if (voteSaved && dataVoteCurrent != null)
            {
                foreach (var item in listBoxCurrent.Items)
                {
                    var dataVote = item as Core.Type.DataVote;
                    if (dataVote != null && dataVote.VoteId == vote.VoteId)
                    {
                        listBoxCurrent.SelectedItem = item;
                        break;
                    }
                }
                foreach (var restaurant in dataVoteCurrent.VoteLane)
                {
                    restaurant.voted = false;
                }
                foreach (var restaurant in vote.VoteLane)
                {
                    var result = dataVoteCurrent.VoteLane.Where(x => x.Id == restaurant.Id).ToList();
                    if (result.Count == 1)
                    {
                        result[0].voted = restaurant.voted;
                    }
                }
                if (selectionSaved)
                {
                    dataGridViewCurrent.ClearSelection();
                    dataGridViewCurrent.Rows[cellRow].Cells[cellCol].Selected = true;
                }
                dataGridViewCurrent.EndEdit();
            }
            else
            {
                dataGridViewCurrent.DataSource = null;
            }
        }

        private void refreshFinished(Core.Type.CentralizedData data)
        {
            bool voteSaved = false;
            bool selectionSaved = false;
            int cellRow = 0, cellCol = 0;
            Core.Type.DataVote vote = null;
            if (dataVoteFinished != null)
            {
                if (dataGridViewFinished.SelectedCells.Count > 0)
                {
                    selectionSaved = true;
                    var cell = dataGridViewFinished.SelectedCells[0];
                    cellRow = cell.RowIndex;
                    cellCol = cell.ColumnIndex;
                }
                vote = dataVoteFinished.Copy();
                voteSaved = true;
            }
            listBoxFinished.DataSource = data.dataFinishedVotes;
            if (voteSaved && dataVoteFinished != null)
            {
                foreach (var item in listBoxFinished.Items)
                {
                    var dataVote = item as Core.Type.DataVote;
                    if (dataVote != null && dataVote.VoteId == vote.VoteId)
                    {
                        listBoxFinished.SelectedItem = item;
                        break;
                    }
                }
                foreach (var restaurant in dataVoteFinished.VoteLane)
                {
                    restaurant.voted = false;
                }
                foreach (var restaurant in vote.VoteLane)
                {
                    var result = dataVoteFinished.VoteLane.Where(x => x.Id == restaurant.Id).ToList();
                    if (result.Count == 1)
                    {
                        result[0].voted = restaurant.voted;
                    }
                }
                if (selectionSaved)
                {
                    dataGridViewFinished.ClearSelection();
                    dataGridViewFinished.Rows[cellRow].Cells[cellCol].Selected = true;
                }
                dataGridViewFinished.EndEdit();
            }
            else
            {
                dataGridViewFinished.DataSource = null;
            }
        }

        private void FormMainClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            client.OnRefresh -= onRefresh;
            client.SendDisconnect();
        }

        bool checkBoxValueChanging = false;
        private void DataGridViewCurrent_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var column = dataGridViewCurrent.Columns[e.ColumnIndex];
            if (column.Name == "Voted")
            {
                if (!checkBoxValueChanging)
                {
                    checkBoxValueChanging = true;
                    var cell = dataGridViewCurrent.Rows[e.RowIndex].Cells[e.ColumnIndex] as DataGridViewCheckBoxCell;
                    if (cell != null)
                    {
                        if ((bool)cell.Value == true)
                        {
                            for (int i = 0; i < dataGridViewCurrent.Rows.Count; i++)
                            {
                                if (i != e.RowIndex)
                                {
                                    dataGridViewCurrent.Rows[i].Cells[e.ColumnIndex].Value = false;
                                }
                            }
                        }
                    }
                    checkBoxValueChanging = false;
                }
            }
        }

        private void DataGridViewCurrent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var column = dataGridViewCurrent.Columns[e.ColumnIndex];
            if (column.Name == "Voted")
            {
                dataGridViewCurrent.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void ToolStripButtonDisconnect_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonVote_Click(object sender, EventArgs e)
        {
            foreach (var vote in dataVoteCurrent.VoteLane)
            {
                if (vote.voted)
                {
                    if (client.SendVote((int)dataVoteCurrent.VoteId, (int)vote.Id))
                    {
                        labelDataSended.Visible = true;
                        Task.Run(() => hideLabelDataSended());
                    }
                }
            }
        }

        private void DataGridViewFinished_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var column = dataGridViewFinished.Columns[e.ColumnIndex];
            if (column.Name == "NumberVote")
            {
                FormListUser formListUser = new FormListUser(dataVoteFinished.VoteLane[e.RowIndex].usersWhoVoteForIt.Select(x => x.Name).ToList());
                formListUser.ShowDialog();
            }
            else if (column.Name == "RestaurantName")
            {
                FormRestaurantOverview formRestaurantOverview = new FormRestaurantOverview(dataVoteFinished.VoteLane[e.RowIndex].RestaurantName, dataVoteFinished.VoteLane[e.RowIndex].restaurantDescription);
                formRestaurantOverview.ShowDialog();
            }
        }

        private void ListBoxFinished_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridViewFinished.DataSource = dataVoteFinished?.VoteLane;
            if(dataVoteFinished.VoteLane.Count > 0)
            {
                var list = dataVoteFinished.VoteLane.ToList();
                list.Sort((x, y) => y.numberVote - x.numberVote);
                int maxVote = list[0].numberVote;
                var result = list.Where(x => x.numberVote == maxVote).Select(x => x.restaurantName);
                labelWinner.Text = labelWinner.Text.Split(':')[0] + ":" + String.Join("-", result);
            }
        }
    }
}
