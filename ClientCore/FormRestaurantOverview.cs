﻿using Core.Type.Db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FormRestaurantOverview : Form
    {
        public FormRestaurantOverview(string restaurantName, string restaurantDescription)
        {
            InitializeComponent();
            labelName.Text = restaurantName;
            richTextBoxDescription.Text = restaurantDescription;
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
