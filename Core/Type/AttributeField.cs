﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Type
{
    class AttributeFieldName : Attribute
    {
        public string FieldName;
        public AttributeFieldName(string fieldName)
        {
            FieldName = fieldName;
        }
    }

    class AttributeFieldId : Attribute
    {
    }
}
