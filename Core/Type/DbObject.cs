﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Reflection;
using System.Text;

namespace Core.Type
{

    public class DbObject
    {
        static public string GetTableNameDb(System.Type type)
        {
            foreach (var attribute in type.GetCustomAttributes(true))
            {
                if (attribute is AttributeFieldName fieldAttribute)
                    return fieldAttribute.FieldName;
            }
            return null;
        }

        public void SetFieldFromDb(DbDataReader reader)
        {
            foreach (var field in this.GetType().GetFields())
            {
                var fieldName = GetFieldName(field);
                if (!String.IsNullOrEmpty(fieldName))
                {
                    int ordinalField = reader.GetOrdinal(fieldName);
                    if (!reader.IsDBNull(ordinalField))
                    {
                        if (field.FieldType == typeof(Nullable<int>))
                        {
                            field.SetValue(this, Convert.ToInt32(reader.GetValue(ordinalField)));
                        }
                        else if (field.FieldType == typeof(Nullable<bool>))
                        {
                            field.SetValue(this, Convert.ToBoolean(reader.GetValue(ordinalField)));
                        }
                        else if (field.FieldType == typeof(string))
                        {
                            field.SetValue(this, reader.GetValue(ordinalField));
                        }
                        else if (field.FieldType == typeof(Nullable<DateTime>))
                        {
                            field.SetValue(this, Convert.ToDateTime(reader.GetValue(ordinalField)));
                        }
                    }
                }
            }
        }

        private string GetFieldName(FieldInfo fieldInfo)
        {
            foreach (var attribute in fieldInfo.GetCustomAttributes(true))
            {
                if (attribute is AttributeFieldName fieldAttribute)
                    return fieldAttribute.FieldName;
            }
            return null;
        }

        public string GetWhereClause()
        {
            string result = "";
            foreach (var field in this.GetType().GetFields())
            {
                if (field.GetValue(this) != null)
                {
                    if (result == "")
                    {
                        result = GetFieldName(field) + " = @W" + GetFieldName(field);
                    }
                    else
                    {
                        result += " AND " + GetFieldName(field) + " = @W" + GetFieldName(field);
                    }
                }
            }
            if (result == "")
                return "TRUE";
            else
                return result;
        }

        public void AddParameterWhere(MySqlParameterCollection parameter)
        {
            foreach (var field in this.GetType().GetFields())
            {
                parameter.AddWithValue("W" + GetFieldName(field), field.GetValue(this));
            }
        }

        public string GetSetClause(bool erase)
        {
            string result = "";
            foreach (var field in this.GetType().GetFields())
            {
                if (field.GetValue(this) != null || erase)
                {
                    if (result == "")
                    {
                        result = GetFieldName(field) + " = @S" + GetFieldName(field);
                    }
                    else
                    {
                        result += ", " + GetFieldName(field) + " = @S" + GetFieldName(field);
                    }
                }
            }
            if (result == "")
                throw new InvalidOperationException("Object empty");
            else
                return result;
        }

        public void AddParameterSet(MySqlParameterCollection parameter)
        {
            foreach (var field in this.GetType().GetFields())
            {
                parameter.AddWithValue("S" + GetFieldName(field), field.GetValue(this));
            }
        }

        public override string ToString()
        {
            string result = "";
            foreach (var field in this.GetType().GetFields())
            {
                if (field.GetValue(this) != null)
                {
                    string value;
                    if (field.FieldType == typeof(Nullable<DateTime>))
                    {
                        var temp = field.GetValue(this) as Nullable<DateTime>;
                        value = temp?.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    }
                    else
                    {
                        value = field.GetValue(this).ToString();
                    }
                    if (result == "")
                    {
                        result = field.Name + " : " + field.GetValue(this);
                    }
                    else
                    {
                        result += result = "\t" + field.Name + " : " + field.GetValue(this);
                    }
                }
            }
            return result;
        }

        public string GetIdClause()
        {
            foreach (var field in this.GetType().GetFields())
            {
                AttributeFieldName resultAttributeName = null;
                FieldInfo resultFieldId = null;
                foreach (var attribute in field.GetCustomAttributes(true))
                {
                    if (attribute is AttributeFieldName fieldAttributeName)
                        resultAttributeName = fieldAttributeName;
                    if (attribute is AttributeFieldId fieldAttributeId)
                        resultFieldId = field;
                }
                if (resultAttributeName != null && resultFieldId != null)
                {
                    return resultAttributeName.FieldName + " = " + resultFieldId.GetValue(this);
                }
            }
            return null;
        }

        public static string GetIdClause(System.Type type, params int[] ids)
        {
            foreach (var field in type.GetFields())
            {
                AttributeFieldName resultAttributeName = null;
                FieldInfo resultFieldId = null;
                foreach (var attribute in field.GetCustomAttributes(true))
                {
                    if (attribute is AttributeFieldName fieldAttributeName)
                        resultAttributeName = fieldAttributeName;
                    if (attribute is AttributeFieldId fieldAttributeId)
                        resultFieldId = field;
                }
                if (resultAttributeName != null && resultFieldId != null)
                {
                    string result = resultAttributeName.FieldName + " = " + ids[0];
                    for (int i = 0; i < ids.Length; i++)
                        result += " AND " + resultAttributeName.FieldName + " = " + ids[i];
                    return result;
                }
            }
            return null;
        }

        public string GetInsertColumnClause()
        {
            string result = "";
            foreach (var field in this.GetType().GetFields())
            {
                if (result == "")
                {
                    result = "(" + GetFieldName(field);
                }
                else
                {
                    result += ", " + GetFieldName(field) ;
                }
            }
            if (result == "")
                throw new InvalidOperationException("Object empty");
            else
                return result + ")";
        }

        public string GetInsertValueClause()
        {
            string result = "";
            foreach (var field in this.GetType().GetFields())
            {
                if (result == "")
                {
                    result = "(@I" + GetFieldName(field);
                }
                else
                {
                    result += ", @I" + GetFieldName(field);
                }
            }
            if (result == "")
                throw new InvalidOperationException("Object empty");
            else
                return result + ")";
        }
        public void AddParameterInsert(MySqlParameterCollection parameter)
        {
            foreach (var field in this.GetType().GetFields())
            {
                parameter.AddWithValue("I" + GetFieldName(field), field.GetValue(this));
            }
        }
    }
}
