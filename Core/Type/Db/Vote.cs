﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Type.Db

{
    [AttributeFieldName("vote")]
    public class Vote : DbObject
    {
        [AttributeFieldId]
        [AttributeFieldName("idvote")]
        public Nullable<int> Id;
        [AttributeFieldName("start")]
        public Nullable<DateTime> Start;
        [AttributeFieldName("end")]
        public Nullable<DateTime> End;
    }
}
