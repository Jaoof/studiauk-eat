﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Type.Db
{
    [AttributeFieldName("voix")]
    public class Voix : DbObject
    {
        [AttributeFieldName("idrestaurant")]
        public Nullable<int> IdRestaurant;
        [AttributeFieldName("iduser")]
        public Nullable<int> IdUser;
        [AttributeFieldName("idvote")]
        public Nullable<int> IdVote;
    }
}
