﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Type.Db
{
    [AttributeFieldName("User")]
    public class User : DbObject
    {
        [AttributeFieldName("iduser")]
        [AttributeFieldId]
        public Nullable<int> Id;
        [AttributeFieldName("username")]
        public string Name;
        [AttributeFieldName("passwordHash")]
        public string PasswordHash;
        [AttributeFieldName("lastConnection")]
        public Nullable<DateTime> LastConnection;
        [AttributeFieldName("confirmed")]
        public Nullable<bool> Confirmed;
        [AttributeFieldName("mail")]
        public string Mail;

        public User DeepCopy()
        {
            var newObj = (User)this.MemberwiseClone();
            return newObj;
        }
    }
}
