﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Type.Db

{
    [AttributeFieldName("restaurant")]
    public class Restaurant : DbObject
    {
        [AttributeFieldId]
        [AttributeFieldName("restaurantId")]
        public Nullable<int> Id;
        [AttributeFieldName("name")]
        public string Name;
        [AttributeFieldName("description")]
        public string Description;
    }
}
