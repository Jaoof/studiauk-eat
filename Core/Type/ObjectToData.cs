﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Core.Type
{
    public class ObjectToData
    {
        public static CentralizedData ListObjectToData(List<Db.Restaurant> restaurants, List<Db.User> users,
            List<Db.Voix> voix, List<Db.Vote> votes, int userTargetId)
        {
            CentralizedData res = new CentralizedData();

            foreach (Db.Vote vote in votes)
            {
                BindingList<LineDataGrid> voteLine = new BindingList<LineDataGrid>();
                foreach (Db.Restaurant restaurant in restaurants)
                {
                    List<Db.Voix> restaurantVoix = voix.FindAll(x => x.IdRestaurant == restaurant.Id && x.IdVote == vote.Id);
                    bool voted = restaurantVoix.Exists(x => x.IdUser == userTargetId);
                    List<Db.User> restaurantVoixUsers = users.FindAll(x => restaurantVoix.Exists(y => y.IdUser == x.Id));
                    List<Db.Vote> winningVote = votes;//.FindAll(x => x.winnerId == restaurant.Id); ICI
                    Type.LineDataGrid line = new LineDataGrid(restaurant, voted,restaurantVoix.Count, winningVote.Count, restaurantVoixUsers);
                    voteLine.Add(line);
                }

                Type.DataVote data = new DataVote {EndTime = vote.End, StartTime = vote.Start, VoteId = vote.Id, VoteLane = voteLine };


                if (DateTime.UtcNow < vote.End) //Vote en cours
                {
                    res.dataCurrentVotes.Add(data);
                }
                else
                    res.dataFinishedVotes.Add(data);
            }

            res.LastUpdate = DateTime.UtcNow;

            return res;
        }
    }
}
