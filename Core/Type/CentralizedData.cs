﻿using Core.Type.Db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace Core.Type
{
    public class LineDataGrid
    {
        public LineDataGrid()
        {
        }
        public LineDataGrid(Restaurant restaurant, bool voted, int numberVote, int numberWinningVote, List<User> usersWhoVoteForIt)
        {
            this.Id = restaurant.Id;
            this.voted = voted;
            this.restaurantName = restaurant.Name;
            this.restaurantDescription = restaurant.Description;
            this.numberVote = numberVote;
            this.numberWinningVote = numberWinningVote;
            this.usersWhoVoteForIt = usersWhoVoteForIt;
        }

        public Nullable<int> Id;
        public bool voted;
        public string restaurantName;
        public string restaurantDescription;
        public int numberVote;
        public List<User> usersWhoVoteForIt;
        public int numberWinningVote;
        public bool Voted { get => voted; set => voted = value; }
        public string RestaurantName { get => restaurantName;}
        public int NumberVote { get => numberVote; }
        //public int NumberWinningVote { get => numberWinningVote; }
    }

    public class DataVote
    {
        public BindingList<LineDataGrid> VoteLane;
        public Nullable<DateTime> StartTime;
        public Nullable<DateTime> EndTime;
        public Nullable<int> VoteId;
        public override string ToString()
        {
            return StartTime?.ToLocalTime().ToString();
        }

        public DataVote Copy()
        {
            return (DataVote)this.MemberwiseClone();
        }
    }
    public class CentralizedData
    {
        public List<DataVote> dataCurrentVotes = new List<DataVote>();
        public List<DataVote> dataFinishedVotes = new List<DataVote>();
        public DateTime LastUpdate;

        public static CentralizedData FromXmlStr(string str)
        {
            System.Xml.Serialization.XmlSerializer reader =
            new System.Xml.Serialization.XmlSerializer(typeof(CentralizedData));
            using (var stringReader = new StringReader(str))
            {
                return (CentralizedData)reader.Deserialize(stringReader);
            }
        }

        public string ToXmlStr()
        {
            System.Xml.Serialization.XmlSerializer writer =
            new System.Xml.Serialization.XmlSerializer(typeof(CentralizedData));
            using (var stringWriter = new StringWriter())
            {
                writer.Serialize(stringWriter, this);
                return stringWriter.ToString();
            }
        }

        static public CentralizedData GetExemple()
        {
            return new Core.Type.CentralizedData
            {
                dataCurrentVotes = new List<Core.Type.DataVote>
                {
                    new Core.Type.DataVote
                    {
                        StartTime = DateTime.UtcNow,
                        VoteLane = new BindingList<Core.Type.LineDataGrid>
                        {
                            new Core.Type.LineDataGrid(new Core.Type.Db.Restaurant{Id = 0, Name ="test"}, false, 0, 0, new List<Core.Type.Db.User>()),
                            new Core.Type.LineDataGrid(new Core.Type.Db.Restaurant{Id = 1, Name ="chez papa"}, true, 10, 55, new List<Core.Type.Db.User>())
                        },
                        EndTime = DateTime.UtcNow.AddHours(3)
                    },
                    new Core.Type.DataVote
                    {
                        StartTime = DateTime.UtcNow.AddDays(-50),
                        VoteLane = new BindingList<Core.Type.LineDataGrid>
                        {
                            new Core.Type.LineDataGrid(new Core.Type.Db.Restaurant{Id = 0, Name ="franxprix"}, false, 0, 0, new List<Core.Type.Db.User>()),
                            new Core.Type.LineDataGrid(new Core.Type.Db.Restaurant{Id = 1, Name ="macdo"}, true, 10, 55, new List<Core.Type.Db.User>())
                        },
                        EndTime = DateTime.UtcNow.AddDays(3)
                    }
                }
            };
        }
    }
}
