﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System.Threading;

namespace Core.Net
{
    public class ServerTcp
    {   
        private List<ClientTcp> tcpClientList = new List<ClientTcp>();
        private Thread _thread;
        private volatile bool _stopThread = false;

        public ServerTcp()
        {
        }

        public void Start()
        {
            _thread = new Thread(new ThreadStart(threadProc));
            _thread.Start();
        }

        public void Stop()
        {
            _stopThread = true;
            _thread.Join();
        }
        private void threadProc()
        {
            TcpListener server = null;

            try
            {
                int port = 13000;
                IPAddress localAddress = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(localAddress, port);

                server.Start();

                Byte[] bytes = new Byte[256];

                while (!_stopThread)
                {
                    ClientTcp tcpClient = new ClientTcp(server.AcceptTcpClient());
                    tcpClient.OnReceive += new ClientTcp.OnReceiveHandler(Client_OnReceive);
                    tcpClientList.Add(tcpClient);
                }
            }
            catch(SocketException e)
            {
                Console.WriteLine("SocketException: " + e);
            }
            finally
            {
                server.Stop();
            }
        }


        public void Client_OnReceive(ClientTcp src, string s)
        {
            foreach(ClientTcp client in tcpClientList)
            {
                if(client != src)
                {
                    client.Send(s);
                }
            }
            if (s.Equals("exit"))
            {
                src.Close();
            }
        }

    }
}
