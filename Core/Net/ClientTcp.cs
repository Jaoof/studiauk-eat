﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace Core.Net
{
    public enum Protocol
    {
        Approved,
        Disapproved,
        Connnection,
        Disconnection,
        Register,
        Vote,
        Refresh,
        NewVote,
        NewRestaurant
    };
    public class ClientTcp
    {
        private TcpClient _client;
        private NetworkStream _stream;
        private Thread _thread;
        private volatile bool _stopThread = false;

        private bool isDisposed = false;
        public bool IsDisposed
        {
            get { return isDisposed; }
        }

        private bool isClosed = false;
        public bool IsClosed { get { return isClosed; } }

        private bool isRunning = false;
        public bool IsRunning { get { return isRunning; } }

        private bool isUserApproved = false;
        public bool IsUserApproved { get { return isUserApproved; } set { isUserApproved = value; } }
        private Nullable<int> userId;
        public Nullable<int> UserId { get { return userId; } set { userId = value; } }

        public OnReceiveHandler OnReceive;
        public delegate void OnReceiveHandler(ClientTcp src, string s);

        public ClientTcp()
        {
            _client = new TcpClient();
        }

        public ClientTcp(string ipString, int port)
        {
            _client = new TcpClient();
            Connect(ipString, port);
        }

        public ClientTcp(TcpClient tcpClient)
        {
            _client = tcpClient;
            isRunning = true;
            _stream = _client.GetStream();
            StartAsyncReading();
        }

        public void Connect(string ipString, int port)
        {
            //if (!isRunning) Close();

            try
            {
                IPAddress ip = IPAddress.Parse(ipString);
                _client.Connect(ip, port);
                _stream = _client.GetStream();
                StartAsyncReading();
                isRunning = true;
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("Argument nulll exceptiion : " + e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException : " + e);
            }

        }

        public void Close()
        {
            _stopThread = true;
            isClosed = true;
            isRunning = false;
            _stream?.Close();
            _client?.Close();
            Console.WriteLine("Client disconected");
        }
        public void Dispose()
        {
            isDisposed = true;
            Close();
            _client?.Dispose();
            Console.WriteLine("Client disposed");
        }

        public void Send(string s)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(s);
            _stream.Write(data, 0, data.Length);

            Console.WriteLine("Data : " + s + " sent");
        }

        public void StartAsyncReading()
        {
            _thread = new Thread(new ThreadStart(AsyncRead));
            _thread.Start();
        }
        private void AsyncRead()
        {
            try
            {
                Byte[] bytes = new Byte[262144];
                String data = null;
                int i;

                while (!_stopThread)
                {
                    while ((i = _stream.Read(bytes, 0, bytes.Length)) != 0 && !_stopThread)
                    {
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received : " + data);
                        OnReceive?.Invoke(this, data);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception Read : " + e);
            }
            finally
            {
                if (!isClosed) Close();
            }
        }

    }
}
