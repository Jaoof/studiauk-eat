﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Core.Type;
using Core.Type.Db;
using MySql.Data.MySqlClient;

namespace Core.Db
{

    public class DbUtills
    {
        readonly MySqlConnection connection;

        public DbUtills()
        {
            string connectionString = "SERVER=localhost; DATABASE=studiaukEat; UID=root; PASSWORD=";
            this.connection = new MySql.Data.MySqlClient.MySqlConnection(connectionString);
        }

        public List<T> GetDbObject<T>()
        {
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    List<DbObject> output = new List<DbObject>();
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "Select * FROM " + DbObject.GetTableNameDb(typeof(T)) + ";";

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DbObject temp = (DbObject)Activator.CreateInstance(typeof(T));
                                temp.SetFieldFromDb(reader);
                                output.Add(temp);
                            }
                        }
                    }
                    return output.ConvertAll<T>(new Converter<DbObject, T>(x => (T)Convert.ChangeType(x, typeof(T))));
                }
                else
                    throw new ArgumentException();
            }
            finally
            {
                this.connection.Close();
            }
            
        }

        public T CompleteObject<T>(T obj)
        {
            this.connection.Open();

            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    DbObject father = (DbObject)Convert.ChangeType(obj, typeof(DbObject));
                    DbObject temp = null;
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "Select * FROM " + DbObject.GetTableNameDb(typeof(T)) + " WHERE " + father.GetWhereClause() + ";";
                    father.AddParameterWhere(cmd.Parameters);

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                temp = (DbObject)Activator.CreateInstance(typeof(T));
                                temp.SetFieldFromDb(reader);
                            }
                        }
                        else
                        {
                            return default;
                        }
                    }
                    return (T)Convert.ChangeType(temp, typeof(T));
                }
                else
                    throw new ArgumentException();
            }
            finally
            {
                this.connection.Close();
            }
        }

        public List<T> FindSimilarObjects<T>(T obj)
        {
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    List<DbObject> output = new List<DbObject>();
                    DbObject father = (DbObject)Convert.ChangeType(obj, typeof(DbObject));
                    DbObject temp = null;
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "Select * FROM " + DbObject.GetTableNameDb(typeof(T)) + " WHERE " + father.GetWhereClause() + ";";
                    father.AddParameterWhere(cmd.Parameters);

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                temp = (DbObject)Activator.CreateInstance(typeof(T));
                                temp.SetFieldFromDb(reader);
                                output.Add(temp);
                            }
                        }
                    }
                    return output.ConvertAll<T>(new Converter<DbObject, T>(x => (T)Convert.ChangeType(x, typeof(T))));
                }
                else
                    throw new ArgumentException();
            }
            finally
            {
                this.connection.Close();
            }
        }

        public List<T> FindObjectByIds<T>(params int[] ids)
        {
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    List<DbObject> output = new List<DbObject>();
                    MySqlCommand cmd = this.connection.CreateCommand();
                    DbObject temp;
                    cmd.CommandText = "Select * FROM " + DbObject.GetTableNameDb(typeof(T)) + " WHERE " + DbObject.GetIdClause(typeof(T), ids) + ";";

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                temp = (DbObject)Activator.CreateInstance(typeof(T));
                                temp.SetFieldFromDb(reader);
                                output.Add(temp);
                            }
                        }
                    }
                    return output.ConvertAll<T>(new Converter<DbObject, T>(x => (T)Convert.ChangeType(x, typeof(T))));
                }
                else
                    throw new ArgumentException();
            }
            finally
            {
                this.connection.Close();
            }
        }

        public void UpdateObject<T>(T objToReplace, T objForReplacement, bool nullStrict)
        {
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    DbObject father = objToReplace as DbObject;
                    DbObject replace = objForReplacement as DbObject;
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "UPDATE " + DbObject.GetTableNameDb(typeof(T)) + " SET " + replace.GetSetClause(nullStrict) + " WHERE " + father.GetWhereClause() + ";";
                    replace.AddParameterSet(cmd.Parameters);
                    father.AddParameterWhere(cmd.Parameters);
                    cmd.ExecuteNonQuery();
                }
                else
                    throw new ArgumentException();
            }
            finally
            {
                this.connection.Close();
            }
        }

        public void UpdateObjectOnId<T>(T objToReplace, T objForReplacement, bool nullStrict)
        {
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    DbObject father = objToReplace as DbObject;
                    DbObject replace = objForReplacement as DbObject;
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "UPDATE " + DbObject.GetTableNameDb(typeof(T)) + " SET " + replace.GetSetClause(nullStrict) + " WHERE " + father.GetIdClause() + ";";
                    replace.AddParameterSet(cmd.Parameters);
                    cmd.ExecuteNonQuery();
                }
                else
                    throw new ArgumentException();
            }
            finally
            {
                this.connection.Close();
            }
        }

        public bool InsertObject<T>(T obj)
        {
            bool output = true;
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    DbObject father = obj as DbObject;
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "INSERT INTO " + DbObject.GetTableNameDb(typeof(T)) + " " + father.GetInsertColumnClause() + " VALUES " + father.GetInsertValueClause() + ";";
                    father.AddParameterInsert(cmd.Parameters);
                    cmd.ExecuteNonQuery();
                }
                else
                    throw new ArgumentException();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                output = false;
            }
            finally
            {
                this.connection.Close();
            }
            return output;
        }

        public void DeleteObject<T>(T obj)
        {
            this.connection.Open();
            try
            {
                if (typeof(T).IsSubclassOf(typeof(DbObject)))
                {
                    DbObject father = obj as DbObject;
                    MySqlCommand cmd = this.connection.CreateCommand();

                    cmd.CommandText = "DELETE FROM " + DbObject.GetTableNameDb(typeof(T)) + " WHERE " + father.GetWhereClause() + ";";
                    father.AddParameterWhere(cmd.Parameters);
                    cmd.ExecuteNonQuery();
                }
                else
                    throw new ArgumentException();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                this.connection.Close();
            }
        }

        public CentralizedData GetCentralizedData(int targetUserId)
        {
            return ObjectToData.ListObjectToData(GetDbObject<Restaurant>(), GetDbObject<User>(), GetDbObject<Voix>(), GetDbObject<Vote>(), targetUserId);
        }
    }
}
